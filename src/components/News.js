
import React from 'react';
import Post from './Post';

import news from '../news'


class News extends React.Component {
  constructor(props){
    super(props)

    this.state = {
        stateNews: news,
        value: '',
    }
    this.myRef = React.createRef()
  }

  reverseData = () =>{
    this.state.stateNews.reverse()  
    this.setState({
      stateNews: this.state.stateNews
    })
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }

  findPost = () =>{
    this.setState({
      stateNews: this.state.stateNews.filter(post=>post.title.toLowerCase() === this.myRef.value.toLowerCase())
    })
  }
  
  discard = () =>{
    this.setState({
      stateNews: news
    })
  }
  
  render = () => {
    return(
      <div className="News">
         <button onClick={this.reverseData}>Перевернуть</button>
         <input type="text" ref={(input)=>this.myRef=input} value={this.state.value} onChange={this.handleChange} />
         <button onClick={this.findPost}>Найти</button>
         <button onClick={this.discard}>Сбросить</button>
        {this.state.stateNews.map((item, index) =>  index === 0 ? <Post news={item} key={item.id} flag/>: <Post news={item} key={item.id} index={index} />)}
      </div>
    )
  }
}
  


export default News;