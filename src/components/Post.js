
import React from 'react';


class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: this.props.flag ? true: false,

    }
  }

  eventHandler = () =>{
    this.setState({
      isOpen: !this.state.isOpen
    })
  }


  componentWillReceiveProps(nextProps){
    if(nextProps.flag !== this.state.isOpen){
      this.setState({
        isOpen: nextProps.flag
      })
    }
  }

  render = () =>{ 
    return(
    
    <div className="Post">
      <h1>{this.props.news.title}</h1>
      <img src={require("./../images/first.jpg").default} height={400} width={400}/>
      <div>{this.props.news.description}</div>
      <button onClick={this.eventHandler}>{this.state.isOpen?"Свернуть":"Развернуть"}</button>
      <div>{this.state.isOpen && this.props.news.text }</div>
    </div>)}
}

export default Post;